FROM golang:1.16 as build-stage

WORKDIR /opt/conductor

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN go build -o conductor -ldflags '-s' ./cmd/conductor


FROM debian:10 as runtime

RUN apt update && apt install -y sqlite3

WORKDIR /opt/conductor

COPY --from=build-stage /opt/conductor/conductor /opt/conductor/conductor

COPY --from=build-stage /opt/conductor/create_tables.sql /opt/conductor/create_tables.sql

RUN sqlite3 conductor.db < /opt/conductor/create_tables.sql

EXPOSE 9090

ENTRYPOINT ["/opt/conductor/conductor"]
