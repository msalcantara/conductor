package main

import (
	"conductor/account"
	"conductor/api"
	"database/sql"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	// _ "conductor/cmd/conductor/docs"
	_ "conductor/docs"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	logger := log.New(os.Stderr, "[conductor] ", log.LstdFlags)

	db, err := sql.Open("sqlite3", "conductor.db")
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error to open database: %v\n", err)
		os.Exit(1)
	}
	defer db.Close()

	wd, err := os.Getwd()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to get current working directory: %v\n", err)
		os.Exit(1)
	}

	accountSvc := account.NewAccountService(db)
	transactionSvc := account.NewTransactionService(db, account.TTFOptions{
		Family: "Roboto-Regular",
		Path:   filepath.Join(wd, "ttf/Roboto-Regular.ttf"),
		Size:   14,
	})

	api := api.New(logger, api.Options{
		Port:          9090,
		TrasactionSvc: transactionSvc,
		AccountSvc:    accountSvc,
	})

	go func() {
		if err := api.Start(); err != nil {
			fmt.Fprintf(os.Stderr, "Error to start API: %v\n", err)
			os.Exit(1)
		}
	}()

	term := make(chan os.Signal, 1)
	signal.Notify(term, os.Interrupt, syscall.SIGTERM)
	<-term
}
