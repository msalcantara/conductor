## Banco de dados
O banco de dados está zipado no arquivo `conductor.db.zip`. Esse banco de dados está com uma amostra pequena de contas e transações(500 contas e 500 transações para cada conta) para não deixar o download do repositório lento, para gerar um banco de dados maior, pode utilizar o script `create_database.sql`:
- `$ rm -f conductor.db && sqlite3 conductor.db < create_database.sql`

## Swagger
O swagger está disponivel na url `http://localhost:9090/swagger/index.html`

