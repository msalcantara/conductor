package api

import (
	"conductor/account"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

type TransactionSerivce interface {
	Transactions(context.Context, string) ([]account.Transaction, error)
	TransactionsPDF(context.Context, string, io.Writer) error
}

type AccountService interface {
	Account(context.Context, string) (account.Account, error)
	Accounts(context.Context) ([]account.Account, error)
}

type apiV1 struct {
	logger      *log.Logger
	transaction TransactionSerivce
	account     AccountService
}

func (api *apiV1) register(r *mux.Router) {
	r.HandleFunc("/contas", api.allAccounts).Methods("GET")
	r.HandleFunc("/contas/{id}", api.findAccount).Methods("GET")
	r.HandleFunc("/contas/{id}/transacoes", api.findAccountTransactions).Methods("GET")
	r.HandleFunc("/contas/{id}/transacoes.pdf", api.downloadAccountTransactions).Methods("GET")
}

// downloadAccountTransactions
// @Summary Download de transações de conta em formato PDF
// @Param id path string true "ID da conta"
// @Produce application/pdf
// @Success 400 {string} message "Transações de conta não encontrada"
// @Failure 500 {string} message "Erro interno ao buscar transações de conta"
// @Router /contas/{id}/transacoes.pdf [get]
func (api *apiV1) downloadAccountTransactions(w http.ResponseWriter, r *http.Request) {
	accountID := mux.Vars(r)["id"]
	name := fmt.Sprintf("transactions-%s.pdf", accountID)

	w.Header().Set("Content-Type", "application/pdf")
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", name))

	file, err := os.CreateTemp(os.TempDir(), name)
	if err != nil {
		http.Error(w, internalServerErrorMsg, http.StatusInternalServerError)
		return
	}
	defer func() {
		if err := file.Close(); err != nil {
			api.logger.Printf("Error to close report file: %v\n", err)
		}
		if err := os.Remove(file.Name()); err != nil {
			api.logger.Printf("Error to remove report file: %v\n", err)
		}
	}()

	if err := api.transaction.TransactionsPDF(r.Context(), accountID, file); err != nil {
		if errors.Is(err, account.ErrTransactionsNotFound) {
			http.Error(w, notFoundObjectMsg, http.StatusInternalServerError)
			return
		}
		api.logger.Printf("Error to generate PDF: %v\n", err)
		http.Error(w, internalServerErrorMsg, http.StatusInternalServerError)
		return
	}

	if _, err := file.Seek(0, 0); err != nil {
		api.logger.Printf("Error to seed file to start: %v\n", err)
		http.Error(w, internalServerErrorMsg, http.StatusInternalServerError)
		return
	}

	if _, err := io.Copy(w, file); err != nil {
		api.logger.Printf("Error to copy from pdf to response: %v\n", err)
		http.Error(w, internalServerErrorMsg, http.StatusInternalServerError)
		return
	}
}

// findAccountTransactions
// @Summary Retorna transações de conta de acordo com ID informado
// @Param id path string true "ID da conta"
// @Produce application/json
// @Success 200 {object} account.Transaction "Transações da conta"
// @Success 400 {object} api.ResponseError "Transações de conta não encontrada"
// @Failure 500 {object} api.ResponseError "Erro interno ao buscar transações de conta"
// @Router /contas/{id}/transacoes [get]
func (api *apiV1) findAccountTransactions(w http.ResponseWriter, r *http.Request) {
	accountID := mux.Vars(r)["id"]
	transactions, err := api.transaction.Transactions(r.Context(), accountID)
	if err != nil {
		if errors.Is(err, account.ErrTransactionsNotFound) {
			api.responseError(w, notFoundObjectMsg, http.StatusBadRequest)
			return
		}
		api.logger.Printf("Error to get account transactions: %v\n", err)
		api.responseError(w, internalServerErrorMsg, http.StatusInternalServerError)
		return
	}
	api.responseSuccess(w, r, transactions, http.StatusOK)
}

// findAccount
// @Summary Retorna conta de acordo com ID informado
// @Param id path string true "ID da conta"
// @Produce application/json
// @Success 200 {object} account.Account "Dados da conta"
// @Failure 400 {object} api.ResponseError "Conta não encontrada"
// @Failure 500 {object} api.ResponseError "Erro interno ao buscar conta"
// @Router /contas/{id} [get]
func (api *apiV1) findAccount(w http.ResponseWriter, r *http.Request) {
	accountID := mux.Vars(r)["id"]
	acc, err := api.account.Account(r.Context(), accountID)
	if err != nil {
		if errors.Is(err, account.ErrAccountNotFound) {
			api.responseError(w, notFoundObjectMsg, http.StatusBadRequest)
			return
		}
		api.logger.Printf("Error to get account: %v\n", err)
		api.responseError(w, internalServerErrorMsg, http.StatusInternalServerError)
		return
	}

	api.responseSuccess(w, r, acc, http.StatusOK)
}

// allAccounts
// @Summary Retorna todas as contas existentes
// @Produce application/json
// @Success 200 {array} account.Account "Lista de todas as contas"
// @Failure 500 {object} api.ResponseError "Erro interno ao buscar contas"
// @Router /contas [get]
func (api *apiV1) allAccounts(w http.ResponseWriter, r *http.Request) {
	accounts, err := api.account.Accounts(r.Context())
	if err != nil {
		api.logger.Printf("Error to get all accounts: %v\n", err)
		api.responseError(w, internalServerErrorMsg, http.StatusInternalServerError)
		return
	}
	api.responseSuccess(w, r, accounts, http.StatusOK)
}

func (api *apiV1) responseSuccess(w http.ResponseWriter, r *http.Request, data interface{}, statusCode int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	if err := json.NewEncoder(w).Encode(data); err != nil {
		api.responseError(w, internalServerErrorMsg, http.StatusInternalServerError)
	}
}

func (api *apiV1) responseError(w http.ResponseWriter, err string, statusCode int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	response := ResponseError{Error: err}
	if err := json.NewEncoder(w).Encode(response); err != nil {
		w.Write([]byte(fmt.Sprintf(`{"error": "%s"}`, err)))
	}
}
