package api

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
	"github.com/urfave/negroni"
)

const (
	internalServerErrorMsg = "internal server error"
	notFoundObjectMsg      = "not found"
)

type ResponseError struct {
	Error string `json:"error"`
}

type Options struct {
	Port          int
	TrasactionSvc TransactionSerivce
	AccountSvc    AccountService
}

// @title Conductor Tecnologia
// @version 1.0
// @description Conductor Tecnologia - Desafio Golang

// @host localhost:9090
// @BasePath /conductor/api/v1
type API struct {
	logger      *log.Logger
	server      *http.Server
	transaction TransactionSerivce
	account     AccountService
}

func New(logger *log.Logger, options Options) *API {
	router := mux.NewRouter()
	router.StrictSlash(true)

	n := negroni.Classic()
	n.UseHandler(router)

	router.PathPrefix("/swagger/").Handler(httpSwagger.WrapHandler)

	v1Router := router.PathPrefix("/conductor/api/v1").Subrouter()
	v1 := apiV1{
		logger:      logger,
		transaction: options.TrasactionSvc,
		account:     options.AccountSvc,
	}
	v1.register(v1Router)

	server := &http.Server{Addr: fmt.Sprintf(":%d", options.Port), Handler: n}
	return &API{
		logger: logger,
		server: server,
	}
}

func (api *API) Start() error {
	api.logger.Printf("Server listening on %s\n", api.server.Addr)
	if err := api.server.ListenAndServe(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			return err
		}
	}
	return nil
}

func (api *API) Close() error {
	api.logger.Printf("Closing api")
	return api.server.Close()
}
