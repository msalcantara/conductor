package testutil

import (
	"conductor/account"
	"path/filepath"
	"runtime"
	"testing"
)

func TTFTestOptions(tb testing.TB) account.TTFOptions {
	_, file, _, _ := runtime.Caller(0)
	return account.TTFOptions{
		Family: "Roboto-Regular",
		Path:   filepath.Join(filepath.Dir(file), "../", "ttf/Roboto-Regular.ttf"),
		Size:   14,
	}
}
