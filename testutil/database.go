package testutil

import (
	"database/sql"
	"fmt"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/require"
)

type DataIngestFn func(tb testing.TB, db *sql.DB)

func NewTestDatabase(tb testing.TB, dataIngest ...DataIngestFn) *sql.DB {
	file, err := os.CreateTemp(os.TempDir(), fmt.Sprintf("test-%s.db", tb.Name()))
	require.Nil(tb, err)

	tb.Cleanup(func() {
		require.Nil(tb, os.Remove(file.Name()))
	})

	db, err := sql.Open("sqlite3", file.Name())

	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS accounts(
		id char(36) PRIMARY KEY,
		status INTEGER
	);
	`)
	require.Nil(tb, err)

	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS transactions(
		id char(36) PRIMARY KEY,
		id_account char(36), 
		value REAL, 
		description TEXT,
		FOREIGN KEY (id_account) REFERENCES accounts(id)
	);
	`)
	require.Nil(tb, err)

	for _, fn := range dataIngest {
		fn(tb, db)
	}

	return db
}
