CREATE TABLE IF NOT EXISTS accounts(
    id char(36) PRIMARY KEY default (lower(hex(randomblob(4))) || '-' || lower(hex(randomblob(2))) || '-4' || substr(lower(hex(randomblob(2))),2) || '-' || substr('89ab',abs(random()) % 4 + 1, 1) || substr(lower(hex(randomblob(2))),2) || '-' || lower(hex(randomblob(6)))), 
    status INTEGER
);

CREATE TABLE IF NOT EXISTS transactions(
    id char(36) PRIMARY KEY default (lower(hex(randomblob(4))) || '-' || lower(hex(randomblob(2))) || '-4' || substr(lower(hex(randomblob(2))),2) || '-' || substr('89ab',abs(random()) % 4 + 1, 1) || substr(lower(hex(randomblob(2))),2) || '-' || lower(hex(randomblob(6)))), 
    id_account char(36), 
    value REAL, 
    description TEXT,
    FOREIGN KEY (id_account) REFERENCES accounts(id)
);

CREATE INDEX id_account_index on transactions(id_account);
