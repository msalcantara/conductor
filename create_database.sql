CREATE TABLE IF NOT EXISTS accounts(
    -- Gerador de UUID simples para facilitar o setup do banco de dados
    id char(36) PRIMARY KEY default (lower(hex(randomblob(4))) || '-' || lower(hex(randomblob(2))) || '-4' || substr(lower(hex(randomblob(2))),2) || '-' || substr('89ab',abs(random()) % 4 + 1, 1) || substr(lower(hex(randomblob(2))),2) || '-' || lower(hex(randomblob(6)))), 
    status INTEGER
);

CREATE TABLE IF NOT EXISTS transactions(
    id char(36) PRIMARY KEY default (lower(hex(randomblob(4))) || '-' || lower(hex(randomblob(2))) || '-4' || substr(lower(hex(randomblob(2))),2) || '-' || substr('89ab',abs(random()) % 4 + 1, 1) || substr(lower(hex(randomblob(2))),2) || '-' || lower(hex(randomblob(6)))), 
    id_account char(36), 
    value REAL, 
    description TEXT,
    FOREIGN KEY (id_account) REFERENCES accounts(id)
);

CREATE INDEX id_account_index on transactions(id_account);

-- Criação de 500 accounts com status ATIVADO
INSERT INTO accounts(status) SELECT 1 from generate_series(1, 500);

-- Criação de 500 accounts com status INATIVADO
INSERT INTO accounts(status) SELECT 0 from generate_series(0, 500);

-- Criação de 1000 transações com descrição Netflix para cada conta criada
INSERT INTO transactions (id_account, description, value)
SELECT
    data.id,
    data.description,
    data.value
FROM (
    SELECT
        id,
        'Netflix' AS description,
        (
            SELECT abs(random() % 1000) * 0.5 AS DECIMAL
        ) AS value
        FROM
            accounts) AS data,
    generate_series(1, 1000) AS generator;

-- Criação de 1000 transações com descrição Apple Store para cada conta criada
INSERT INTO transactions (id_account, description, value)
SELECT
    data.id,
    data.description,
    data.value
FROM (
    SELECT
        id,
        'Apple Store' AS description,
        (
            SELECT abs(random() % 1000) * 0.5 AS DECIMAL
        ) AS value
        FROM
            accounts) AS data,
    generate_series(1, 1000) AS generator;

-- Criação de 1000 transações com descrição Amazon para cada conta criada
INSERT INTO transactions (id_account, description, value)
SELECT
    data.id,
    data.description,
    data.value
FROM (
    SELECT
        id,
        'Amazon' AS description,
        (
            SELECT abs(random() % 1000) * 0.5 AS DECIMAL
        ) AS value
        FROM
            accounts) AS data,
    generate_series(1, 1000) AS generator;
