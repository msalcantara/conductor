package account_test

import (
	"conductor/account"
	"conductor/testutil"
	"context"
	"database/sql"
	"os"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGenerateTransactionsPDF(t *testing.T) {
	mockAccount, ingestFnAccount := createMockAccount()

	accountID := mockAccount.ID
	_, ingestFnTransaction := createMockTransactions(accountID)

	db := testutil.NewTestDatabase(t, ingestFnAccount, ingestFnTransaction)

	svc := account.NewTransactionService(db, testutil.TTFTestOptions(t))

	file, err := os.CreateTemp(os.TempDir(), t.Name())
	require.Nil(t, err)
	defer os.Remove(file.Name())

	err = svc.TransactionsPDF(context.Background(), accountID, file)
	require.Nil(t, err)
}

func TestFindAccountTransactions(t *testing.T) {
	mockAccount, ingestFnAccount := createMockAccount()

	accountID := mockAccount.ID
	mockTransactions, ingestFnTransaction := createMockTransactions(accountID)

	db := testutil.NewTestDatabase(t, ingestFnAccount, ingestFnTransaction)

	transactionService := account.NewTransactionService(db, testutil.TTFTestOptions(t))

	transactions, err := transactionService.Transactions(context.Background(), accountID)
	require.Nil(t, err)

	assert.Equal(t, mockTransactions, transactions)

}

func createMockTransactions(accountID string) ([]account.Transaction, testutil.DataIngestFn) {
	mockTransactions := []account.Transaction{
		{
			ID:          uuid.NewString(),
			AccountID:   accountID,
			Description: "testing",
			Value:       199.50,
		},
		{
			ID:          uuid.NewString(),
			AccountID:   accountID,
			Description: "testing",
			Value:       27.50,
		},
		{
			ID:          uuid.NewString(),
			AccountID:   accountID,
			Description: "testing",
			Value:       258.37,
		},
	}

	return mockTransactions, func(tb testing.TB, db *sql.DB) {
		query := "INSERT INTO transactions(id, id_account, description, value) values($1, $2, $3, $4)"
		for _, tr := range mockTransactions {
			_, err := db.Exec(query, tr.ID, tr.AccountID, tr.Description, tr.Value)
			require.Nil(tb, err)
		}
	}

}
