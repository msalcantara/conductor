package account

import (
	"database/sql/driver"
	"fmt"
)

type Status string

const (
	StatusActive   Status = "ATIVADO"
	StatusInactive Status = "INATIVADO"
)

func (s Status) Value() (driver.Value, error) {
	switch s {
	case StatusActive:
		return int64(1), nil
	case StatusInactive:
		return int64(0), nil
	default:
		return nil, fmt.Errorf("invalid status %s", s)
	}
}

func (s *Status) Scan(value interface{}) error {
	if value == nil {
		return fmt.Errorf("nil value")
	}

	code, ok := value.(int64)
	if !ok {
		return fmt.Errorf("could not cast status code to int64")
	}
	switch code {
	case 1:
		*s = StatusActive
	case 0:
		*s = StatusInactive
	default:
		return fmt.Errorf("invalid code %d", code)
	}
	return nil
}

type Account struct {
	ID     string `json:"id"`
	Status Status `json:"status"`
}

type Transaction struct {
	ID          string  `json:"id"`
	AccountID   string  `json:"account_id"`
	Description string  `json:"description"`
	Value       float64 `json:"value"`
}
