package account

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"strconv"

	"github.com/signintech/gopdf"
)

const (
	totalPageLines = 25
)

var (
	ErrTransactionsNotFound = errors.New("account transactions not found")
)

type TTFOptions struct {
	Family string
	Path   string
	Style  string
	Size   int
}

type TransactionService struct {
	db        *sql.DB
	pdfConfig *gopdf.Config
	ttf       TTFOptions
}

func NewTransactionService(db *sql.DB, ttf TTFOptions) *TransactionService {
	return &TransactionService{
		db:        db,
		pdfConfig: &gopdf.Config{PageSize: *gopdf.PageSizeA4},
		ttf:       ttf,
	}
}

func (t *TransactionService) Transactions(ctx context.Context, accountID string) ([]Transaction, error) {
	query := `
	SELECT id, id_account, description, value FROM transactions WHERE id_account = $1
	`

	rows, err := t.db.QueryContext(ctx, query, accountID)
	if err != nil {
		return nil, fmt.Errorf("exec transactions query: %w", err)
	}

	transactions := make([]Transaction, 0)

	for rows.Next() {
		var t Transaction
		if err := rows.Scan(&t.ID, &t.AccountID, &t.Description, &t.Value); err != nil {
			return nil, fmt.Errorf("scan transactions: %w", err)
		}
		transactions = append(transactions, t)
	}

	if len(transactions) == 0 {
		return nil, ErrTransactionsNotFound
	}

	return transactions, nil
}

func (t *TransactionService) TransactionsPDF(ctx context.Context, accountID string, out io.Writer) error {
	transactions, err := t.Transactions(ctx, accountID)
	if err != nil {
		return fmt.Errorf("find account transactions: %w", err)
	}
	pdf := gopdf.GoPdf{}
	pdf.Start(*t.pdfConfig)
	pdf.AddPage()

	if err := pdf.AddTTFFont(t.ttf.Family, t.ttf.Path); err != nil {
		return fmt.Errorf("add ttf font: %w", err)
	}
	if err := pdf.SetFont(t.ttf.Family, t.ttf.Style, t.ttf.Size); err != nil {
		return fmt.Errorf("set font: %w", err)
	}

	initTable := pdf.GetX()
	accountPos := 100.0
	descriptionPos := 220.0
	descriptionPosX := initTable + accountPos + descriptionPos
	valuePos := 130.0
	valuePosX := initTable + accountPos + descriptionPos + valuePos

	pdf.SetX(initTable + accountPos)
	pdf.Cell(nil, "Conta")

	pdf.SetX(descriptionPosX)
	pdf.Cell(nil, "Transação")

	pdf.SetX(valuePosX)
	pdf.Cell(nil, "Valor")

	pageLines := 0
	for _, tr := range transactions {
		if pageLines >= totalPageLines {
			pdf.AddPage()
			pageLines = 0
		}
		pdf.Br(30)

		pdf.SetX(initTable)
		pdf.Cell(nil, tr.ID)

		pdf.SetX(descriptionPosX)
		pdf.Cell(nil, tr.Description)

		pdf.SetX(valuePosX)
		pdf.Cell(nil, strconv.FormatFloat(tr.Value, 'f', 2, 64))

		pageLines++
	}

	return pdf.Write(out)
}
