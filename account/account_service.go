package account

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
)

var (
	ErrAccountNotFound = errors.New("account not found")
)

type AccountService struct {
	db *sql.DB
}

func NewAccountService(db *sql.DB) *AccountService {
	return &AccountService{
		db: db,
	}
}

func (a *AccountService) Account(ctx context.Context, id string) (Account, error) {
	query := `SELECT * from accounts where id = $1`

	var account Account
	row := a.db.QueryRowContext(ctx, query, id)
	if err := row.Scan(&account.ID, &account.Status); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return Account{}, ErrAccountNotFound
		}
		return Account{}, fmt.Errorf("scan row: %w", err)
	}

	return account, nil
}

func (a *AccountService) Accounts(ctx context.Context) ([]Account, error) {
	query := `SELECT * from accounts`

	rows, err := a.db.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("exec query: %w", err)
	}

	accounts := make([]Account, 0)

	for rows.Next() {
		var account Account
		if err := rows.Scan(&account.ID, &account.Status); err != nil {
			return nil, fmt.Errorf("scan row: %w", err)
		}
		accounts = append(accounts, account)
	}

	return accounts, nil
}
