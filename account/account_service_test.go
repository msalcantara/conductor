package account_test

import (
	"conductor/account"
	"conductor/testutil"
	"context"
	"database/sql"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestFindSingleAccount(t *testing.T) {
	mockAccounts, ingestFn := createMockAccounts()
	mockAccount := mockAccounts[0]

	testcases := []struct {
		name    string
		id      string
		account account.Account
		err     error
	}{
		{
			name:    "GetValidAccount",
			id:      mockAccount.ID,
			account: mockAccount,
			err:     nil,
		},
		{
			name:    "GetInvalidAccount",
			id:      uuid.NewString(),
			account: account.Account{},
			err:     account.ErrAccountNotFound,
		},
	}

	db := testutil.NewTestDatabase(t, ingestFn)
	accountService := account.NewAccountService(db)

	for _, tt := range testcases {
		t.Run(tt.name, func(t *testing.T) {
			account, err := accountService.Account(context.Background(), tt.id)
			assert.Equal(t, tt.err, err)
			assert.Equal(t, tt.account, account)
		})

	}

}

func TestFindMultipleAccounts(t *testing.T) {
	mockAccounts, ingestFn := createMockAccounts()
	db := testutil.NewTestDatabase(t, ingestFn)
	accountService := account.NewAccountService(db)
	accounts, err := accountService.Accounts(context.Background())

	require.Nil(t, err)
	assert.Equal(t, mockAccounts, accounts)
}

func createMockAccount() (account.Account, testutil.DataIngestFn) {
	mocks, fn := createMockAccounts()
	return mocks[0], fn
}

func createMockAccounts() ([]account.Account, testutil.DataIngestFn) {
	mockAccounts := []account.Account{
		{
			ID:     uuid.NewString(),
			Status: account.StatusActive,
		},
		{
			ID:     uuid.NewString(),
			Status: account.StatusActive,
		},
		{
			ID:     uuid.NewString(),
			Status: account.StatusInactive,
		},
		{
			ID:     uuid.NewString(),
			Status: account.StatusInactive,
		},
	}

	return mockAccounts, func(tb testing.TB, db *sql.DB) {
		query := "INSERT INTO accounts(id, status) values ($1, $2)"
		for _, acc := range mockAccounts {
			_, err := db.Exec(query, acc.ID, acc.Status)
			require.Nil(tb, err)
		}
	}
}
